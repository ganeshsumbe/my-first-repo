package com.example.service;

import com.example.entity.Department;
import com.example.repository.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public Department save(Department department) {
        log.info("inside department service");
        return departmentRepository.save(department);
    }

    @Override
    public Department getDepartment(Long departmentId) {
        log.info("inside department service");
        return departmentRepository.findById(departmentId).get();
    }
}
