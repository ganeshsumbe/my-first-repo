package com.example.controller;

import com.example.entity.Department;
import com.example.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    DepartmentService departmentService;

    @PostMapping("/")
    public Department saveDepartment(@RequestBody Department department) {
        System.out.println("Caller department service");
        return departmentService.save(department);
    }

    @GetMapping("/{departmentId}")
    public Department getDepartment(@PathVariable("departmentId") Long departmentId) {
        System.out.println("Caller department service");
        return departmentService.getDepartment(departmentId);
    }
}
